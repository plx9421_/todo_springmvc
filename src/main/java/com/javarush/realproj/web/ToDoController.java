package com.javarush.realproj.web;



import java.util.Map;

import org.hibernate.loader.custom.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.javarush.realproj.domain.TodoTask;
import com.javarush.realproj.service.TodoTaskService;

@Controller
public class ToDoController {
	private int viewProperty = 0;
	private TodoTask todoTaskEdit = new TodoTask();
	private int editProperty = 0;

	@Autowired
	private TodoTaskService todoTaskService;

	@RequestMapping("/index")
	public String listTodoTask(Map<String, Object> map) {

		map.put("todoTask", todoTaskEdit);
		map.put("TodoTaskList", todoTaskService.getFilteredTodoTask(viewProperty));

		return "todo";
	}

	@RequestMapping("/")
	public String home() {
		viewProperty = 0;
		return "redirect:/index";
	}

	@RequestMapping("/completed")
	public String todoTaskCompleted() {
		viewProperty = 2;
		return "redirect:/index";
	}

	@RequestMapping("/onwork")
	public String todoTaskInWork() {
		viewProperty = 1;
		return "redirect:/index";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addTodoTask(@ModelAttribute("todoTask") TodoTask todoTask, BindingResult result) {

		if (todoTask.getName() == null || todoTask.getName().isEmpty()) return "redirect:/index";
		if (todoTask.getDescription() == null || todoTask.getDescription().isEmpty()) return "redirect:/index";
		if (todoTask.isCompleted() == null) {
			todoTask.setCompleted(false);
		} 
		if (editProperty == 1){
			todoTask.setId(todoTaskEdit.getId());
			todoTask.setCompleted(todoTaskEdit.isCompleted());
			editProperty = 0;
		}
		todoTaskService.addTodoTask(todoTask);
		todoTaskEdit = new TodoTask();

		return "redirect:/index";
	}

	@RequestMapping("/edit/{todoTaskId}")
	public String editTodoTask(@PathVariable("todoTaskId") Integer todoTaskId) {

		todoTaskEdit = todoTaskService.getTodoTaskById(todoTaskId);
		editProperty = 1;
		
		return "redirect:/index";
		
//		return "edit";
	}

	@RequestMapping("/delete/{todoTaskId}")
	public String deleteTodoTask(@PathVariable("todoTaskId") Integer todoTaskId) {

		todoTaskService.removeTodoTaskById(todoTaskId);

		return "redirect:/index";
	}

	@RequestMapping("/completed/{todoTaskId}")
	public String completedTodoTask(@PathVariable("todoTaskId") Integer todoTaskId) {

		TodoTask todoTask = todoTaskService.getTodoTaskById(todoTaskId);
		todoTask.setCompleted(!todoTask.isCompleted());
		todoTaskService.addTodoTask(todoTask);

		return "redirect:/index";
	}

}
