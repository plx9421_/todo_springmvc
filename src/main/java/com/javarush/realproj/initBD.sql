CREATE USER todomanager@localhost identified BY 'root';
GRANT usage ON *.* TO todomanager@localhost identified BY 'root';
CREATE DATABASE IF NOT EXISTS todotaskbd;
GRANT ALL privileges ON todotaskbd.* TO todomanager@localhost;
USE todotaskbd;
CREATE TABLE `todotask` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(450) NOT NULL,
  `completed` int(10) unsigned zerofill NOT NULL DEFAULT '0000000000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
                                                                                           
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 1", 0, "Системы управления версиями");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 2", 1, "Maven. WAR. Веб-контейнер Tomcat. Сервлеты. Логгирование");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 3", 0, "Обзор Spring Framework. Spring Context. Слои приложения. Создание каркаса приложения.");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 4", 1, "Spring context, JUnit, ORM");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 5", 0, "Hibernate. JPA.");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 6", 1, "Транзакции. Профили Maven и Spring. Пулы коннектов. Spring Data JPA. Spring кэш");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 7", 0, "Кэш Hibernate. Spring Web MVC");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 8", 1, "REST");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 9", 0, "Dandelion. Bootstrap. Datatables. AJAX. jQuery. Spring Security.");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 10", 1,  "Шифрование пароля. Binding. Spring Security Test.");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 11", 0, "CSRF. JSTL. Taglib.");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 12", 1, "Деплой в PaaS-платформу Heroku.");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 13", 0, "Системы управления версиями");
INSERT INTO todotask (name, completed, description) VALUES ("Неделя 14", 1, "Системы управления версиями");
