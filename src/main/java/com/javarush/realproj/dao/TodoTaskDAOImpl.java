package com.javarush.realproj.dao;

import java.util.List;

import com.javarush.realproj.domain.TodoTask;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TodoTaskDAOImpl implements TodoTaskDao{
	@Autowired
	private SessionFactory sessionFactory;


	@Override
	public void addTodoTask(TodoTask task) {
		sessionFactory.getCurrentSession().saveOrUpdate(task);	
	}

	@Override
	public void removeTodoTaskById(Integer id) {
		TodoTask task = (TodoTask) sessionFactory.getCurrentSession().load(TodoTask.class, id);
		if (null != task) {
			sessionFactory.getCurrentSession().delete(task);
		}
	}

	@Override	
	public TodoTask getTodoTaskById(Integer id) {
		Query q = sessionFactory.getCurrentSession().createQuery("from TodoTask where id = :id");
        q.setLong("id", id);
        return (TodoTask) q.uniqueResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TodoTask> getAllTodoTask() {
		return sessionFactory.getCurrentSession().createQuery("from TodoTask").list();
	}

}
