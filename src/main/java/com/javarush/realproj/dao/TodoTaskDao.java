package com.javarush.realproj.dao;

import java.util.List;

import com.javarush.realproj.domain.TodoTask;

public interface TodoTaskDao {

    void addTodoTask(TodoTask task);

    void removeTodoTaskById(Integer id);

    TodoTask getTodoTaskById(Integer id);

    List<TodoTask> getAllTodoTask();

}
