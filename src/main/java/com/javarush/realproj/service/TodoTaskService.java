package com.javarush.realproj.service;

import java.util.List;

import com.javarush.realproj.domain.TodoTask;

public interface TodoTaskService {
	
	void addTodoTask(TodoTask task);

    void removeTodoTaskById(Integer id);

    TodoTask getTodoTaskById(Integer id);

    List<TodoTask> getAllTodoTask();
    
    List<TodoTask> getFilteredTodoTask(int flag);
}
