package com.javarush.realproj.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.javarush.realproj.dao.TodoTaskDao;
import com.javarush.realproj.domain.TodoTask;

@Service
@Transactional
public class TodoTaskServiceImpl implements TodoTaskService {

	@Autowired
	private TodoTaskDao todoTaskDAO;

	@Override	
	public void addTodoTask(TodoTask task) {

		if (task.isCompleted() == null){
			task.setCompleted(false);		
		}
		todoTaskDAO.addTodoTask(task);

	}

	@Override	
	public void removeTodoTaskById(Integer id) {
		todoTaskDAO.removeTodoTaskById(id);
	}

	@Override
	public TodoTask getTodoTaskById(Integer id) {
		return todoTaskDAO.getTodoTaskById(id);
	}

	@Override
	public List<TodoTask> getAllTodoTask() {
		return todoTaskDAO.getAllTodoTask();
	}

	@Override
	public List<TodoTask> getFilteredTodoTask(int flagView) {
		ArrayList<TodoTask> arrayList = new ArrayList<TodoTask>(todoTaskDAO.getAllTodoTask());
		ArrayList<TodoTask> result = new ArrayList<TodoTask>();

		if (flagView != 0){
			for(TodoTask t : arrayList){
				if (flagView == 1 && !t.isCompleted()){
					result.add(t);
					continue;
				}
				if (flagView == 2 && t.isCompleted()){
					result.add(t);
					continue;
				}
			}
		}  else {
			return arrayList;	
		}

		return result;
	}


}
