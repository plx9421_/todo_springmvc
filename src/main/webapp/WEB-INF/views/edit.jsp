<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<title><spring:message code="label.title" /></title>
</head>
<body>

	<h2>
		<spring:message code="label.title" />
	</h2>

	<form:form method="post" action="add" commandName="todoTask">

		<table>
			<tr>
				<td><form:label path="name">
						<spring:message code="label.name" />
					</form:label></td>
				<td><form:input path="name" /></td>
			</tr>
			<tr>
				<td><form:label path="description">
						<spring:message code="label.description" />
					</form:label></td>
				<td><form:input path="description" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit"
					value="<spring:message code="label.addTask"/>" /></td>
			</tr>
		</table>
	</form:form>
	<table>
		<td><a href="<c:url value="/" />"> <spring:message
					code="label.all" />
		</a></td>
		<td><a href="<c:url value="/completed" />"> <spring:message
					code="label.completed" />
		</a></td>
		<td><a href="<c:url value="/onwork" />"> <spring:message
					code="label.onwork" />
		</a></td>
	</table>
	<h3>
		<spring:message code="label.contacts" />
	</h3>
	<c:if test="${!empty TodoTaskList}">
		<table class="data">
			<tr>
				<th><spring:message code="label.name" /></th>
				<th><spring:message code="label.description" /></th>
				<th>&nbsp;</th>
			</tr>
			<c:forEach items="${TodoTaskList}" var="todoTask">
				<tr>
					<td>${todoTask.name}</td>
					<td>${todoTask.description}</td>

					<td><a href="edit/${todoTask.id}"><spring:message
								code="label.edit" /></a></td>

					<td><a href="completed/${todoTask.id}"><spring:message
								code="label.completed" /></a></td>

					<td><a href="delete/${todoTask.id}"><spring:message
								code="label.delete" /></a></td>
				</tr>
			</c:forEach>
		</table>
	</c:if>

</body>
</html>